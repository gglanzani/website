---
layout: home
title: Contact
---
You can reach me via plain old email, via giovanni @ this domain.

Or, if you can fit everything into 130 characters, you could
use [twitter](http://www.twitter.com/gglanzani) or
[app.net](http://alpha.app.net/lanzani).

As a incurable nerd, I'm also on other services, like

+ [Github]
+ [Pinboard]
+ [Flickr]
+ [LinkedIn]

[Github]: https://github.com/gglanzani
[Pinboard]: https://pinboard.in/u:gglanzani/
[Flickr]: https://www.flickr.com/photos/gglanzani/
[LinkedIn]: nl.linkedin.com/pub/giovanni-lanzani/24/618/195



<script>
$( ".contact-active" ).addClass( "active" );
</script>
