---
layout: home
title: CV
---

<img class="pure-img" src="cv.jpg">
The guy on the photo is me, in Lisboa, Portugal. I am smiling, like most of the
time.


<h1 class="content-subhead">Current position</h1>

<button class="button-job pure-button">Data Whisperer</button> <button class="button-company pure-button">GoDataDriven</button> <button class="button-time pure-button">10/2013--Now</button> <button class="button-location pure-button">The Netherlands</button>


At GoDataDriven I enjoy what probably is the coolest job title I'll ever have:
**Data Whisperer**. As for my daily tasks, they involve advising clients on how
to extract valuable insights from (big) data analysis. Being a swell chap, once
that is done, we perform these analysis and build data driven products with a
stellar [team](http://www.godatadriven.com/team.html).

I am also a certified Cloudera trainer for the Data Analyst, Developer and Admin Cloudera courses.


<h1 class="content-subhead">Experience</h1>


<button class="button-job pure-button">IT Consultant</button> <button class="button-company pure-button">KPMG</button> <button class="button-time pure-button">10/2012--09/2013</button> <button class="button-location pure-button">The Netherlands</button>

At KPMG I was mainly doing source code analysis and code quality reviews.
Occasionally involved in the Big Data group and overall always happy to help
when some advanced modeling or analytics are needed.

<button class="button-job pure-button">PhD, Th. Physics</button> <button class="button-company pure-button">Leiden University</button> <button class="button-time pure-button">07/2008--09/2012</button> <button class="button-location pure-button">Leiden</button>

I focused on models describing DNA mechanics, taught Statistical Physics and
Stochastic processes to Master students and had a lot of fun.

While doing that I programmed, wrote papers and delivered stunning
presentations. You don't believe me? I think we should talk.

<button class="button-job pure-button">Research Assistant</button> <button class="button-company pure-button">Leiden University</button> <button class="button-time pure-button">08/2007--07/2008</button> <button class="button-location pure-button">Leiden</button>

It was a fake job. In the pre-crisis years, Leiden University thought to have a
lot of money, so it was paying me to work on my Master thesis. Crazy, right?


<h1 class="content-subhead">Education</h1>

<button class="button-time pure-button">2006--2008</button> <button class="button-job pure-button">MSc Th. Phsyics</button> <button class="button-company pure-button">Leiden University</button> <button class="button-location pure-button">Leiden</button>

Exams average: 8.5/10. Total: 8.25/10 (It's lower than the exams, I KNOW!)

<button class="button-time pure-button">2003--2006</button> <button class="button-job pure-button">BSc Phsyics</button> <button class="button-company pure-button">Università degli studi di Padova</button> <button class="button-location pure-button">Padua (IT)</button>

Getting my Physics bachelor in Padua means I became *Doctor* (in Italy I became a doctor with the
Bachelor. Now you know why they're in recession).

Exam average: 100/110. Total: 105/110.


<h1 class="content-subhead">Languages</h1>

That'd be a long list, but long story short I am very good in English and
Dutch, and less so in Spanish and French. Annoyingly good in Italian.


<h1 class="content-subhead">Computer skills</h1>

<button class="button-company pure-button">Programming languages</button> python (especially Numpy and pandas), C++, C, Haskell, Javascript, Erlang;

<button class="button-company pure-button">Markup languages</button> LaTeX, html + CCS, XML;

<button class="button-company pure-button">Other</button> Hive, Pig, Impala, Hadoop (streaming and non streaming API), PostgreSQL, MySQL, Spark.


<h1 class="content-subhead">Interests</h1>

I have a passion for photography and my favorite subjects are my wife and my four kids.

Furthermore I like reading, writing, playing the guitar and music in general.


<script>
$( ".cv-active" ).addClass( "menu-item-divided pure-menu-selected" );
</script>

